#!/bin/sh

set -eu

BUILD=no
UPLOAD=no
PROGRAM_NAME=$(basename $0)
case "$PROGRAM_NAME" in
  build-and-upload)
    BUILD=yes
    UPLOAD=yes
    ;;
  build)
    BUILD=yes
    UPLOAD=no
    ;;
  upload)
    BUILD=no
    UPLOAD=yes
    ;;
  test)
    BUILD=no
    UPLOAD=no
    ;;
esac

if ! [ -e debian/changelog ]; then
  echo "Not inside a Debian package!"
  exit 1
fi

if [ -x debian/release-check ]; then
  if ! debian/release-check; then
    echo "aborting release because debian/release-check failed"
    exit 1
  fi
fi

highlight() {
  msg="$@"
  printf "\033[01;33m${msg}\033[m\n"
}

banner() {
  echo # blank line
  highlight "========================================================================"
  highlight "= %s" "$@"
  highlight "========================================================================"
  echo # blank line
}

warning() {
  msg="$@"
  printf "\n"
  printf "\033[0;37;41mWARNING: ${msg}\033[m\n"
  printf "\n"
}

run_autopkgtest_lxc() {
  banner "Running autopkgtest with lxc backend (default)"
  if ! sudo lxc-ls -1 | grep -q "^autopkgtest-${autopkgtest_dist}-${arch}\$"; then
    if which distro-info >/dev/null; then
      autopkgtest_dist=$(distro-info --alias=$autopkgtest_dist)
    else
      echo ""
      echo "E: lxc container autopkgtest-$autopkgtest_dist-$arch not found"
      echo "I: Install distro-info so we can also try mapping codenames (toy story characters) to aliases (stable, testing, etc)"
      exit 1
    fi
  fi

  autopkgtest --user debci --apt-upgrade $extra_args "$@" -- lxc --sudo autopkgtest-"$autopkgtest_dist"-"$arch"
}
run_autopkgtest_schroot() {
  banner "Running autopkgtest with schroot backend"
  if ! schroot -l | grep -q "*:${autopkgtest_dist}-${arch}-sbuild\$"; then
    if which distro-info >/dev/null; then
      autopkgtest_dist=$(distro-info --alias=$autopkgtest_dist)
    else
      echo ""
      echo "E: chroot $autopkgtest_dist-$arch-sbuild not found"
      echo "I: Install distro-info so we can also try mapping codenames (toy story characters) to aliases (stable, testing, etc)"
      exit 1
    fi
  fi

  autopkgtest --apt-upgrade $extra_args "$@" -- schroot "$autopkgtest_dist"-"$arch"-sbuild
}

run_autopkgtest() {
  local autopkgtest_dist
  local extra_args
  extra_args=
  autopkgtest_dist="$distribution"
  if [ "$autopkgtest_dist" = unstable -o "$autopkgtest_dist" = UNRELEASED -o "$autopkgtest_dist" = 'experimental' ]; then
    autopkgtest_dist=sid
  fi
  case $autopkgtest_dist in
    *-security)
      autopkgtest_dist=${autopkgtest_dist%%-security}
      ;;
    *-backports)
      extra_args="--add-apt-release=$autopkgtest_dist"
      autopkgtest_dist=${autopkgtest_dist%%-backports}
      ;;
  esac

  local rc
  rc=0
  if [ "${AUTOPKGTEST_VIRT_SERVER:-lxc}" = "schroot" ]; then
    run_autopkgtest_schroot "$@" || rc=$?
  elif [ "${AUTOPKGTEST_VIRT_SERVER:-lxc}" = "lxc" ]; then
    run_autopkgtest_lxc "$@" || rc=$?
  else
    echo "${AUTOPKGTEST_VIRT_SERVER} autopkgtest backend is not supported"
    exit 1
  fi
  [ "$rc" -eq 0 -o "$rc" -eq 2 -o "$rc" -eq 8 ]
}

# check
check_package() {
  banner "Changelog"
  dpkg-parsechangelog

  banner "Lintian checks"
  if ! lintian -Ii; then
    warning "Lintian reported ERRORS in the package!"
  fi

  if grep-dctrl -q -F Build-Depends gem2deb debian/control; then
    banner "Rubygems integration"
    debc | grep 'gemspec$' | sed 's/.*\s//'
  fi

  if [ -f debian/tests/control ] || grep -q '^\(XS-\)\?Testsuite:' debian/control; then
    banner "This package has a test suite!"
    if [ -n "${TEST:-}" ] || confirm "Run the test suite now? [Y/n]" y; then
      local output_dir
      output_dir="${tmpdir}/autopkgtest-self/"
      if ! run_autopkgtest --output-dir="$output_dir" $global_autopkgtest_debs "$changes"; then
        warning "Test suite failed! Please verify before uploading"
        echo "I: autopkgtest output is in $output_dir"
      fi
    else
      echo "OK, but you should probably run the test suite before uploading!"
    fi
  fi

  if [ "$distribution" = unstable ] || \
    [ "$distribution" = experimental ] || \
    [ "$distribution" = UNRELEASED ]; then
    update_chdist
    test_reverse_dependencies
  fi
}

update_chdist() {
  if [ ! -d "${HOME}/.chdist/unstable" ]; then
    chdist create unstable
  fi
  cat > "${HOME}/.chdist/unstable/etc/apt/sources.list" <<EOF
deb http://deb.debian.org/debian/ unstable main contrib
deb-src http://deb.debian.org/debian/ unstable main contrib
EOF
  chdist apt-get unstable update -q2
}

get_reverse_dependencies() {
  local binaries="$(dh_listpackages)"
  local debs="$(dcmd --deb "$changes")"
  local rdeps="$(chdist apt-cache unstable rdepends $binaries | sed -e '/^\s/!d; s/|//' | sort -u | xargs chdist apt-cache unstable show | grep-dctrl -s Package,Source '' | awk '{ if ($1=="Package:") { pkg = $2 }; if ($1=="Source:") { pkg = $2 }; if ($1=="") { print(pkg) } } END { print(pkg) }' | sort -u | grep -v "^${source}\$")"
  local failed_rdeps=""
  if [ -n "$rdeps" ]; then
    testable=""
    untestable=""
    for pkg in $rdeps; do
      if chdist apt-cache unstable showsrc "$pkg" | grep -q ^Testsuite:; then
        echo "$pkg autopkgtest"
      else
        echo "$pkg no-autopkgtest"
      fi
    done
  fi
}

get_reverse_test_dependencies() {
  local pkgregex="$(dh_listpackages | awk -vORS='\\|' '{print($1)}' | sed -e 's/\\|$//')"
  chdist apt-get unstable indextargets --format '$(FILENAME)' "Created-By: Sources" "Suite: unstable" | xargs /usr/lib/apt/apt-helper cat-file | grep-dctrl -F Testsuite-Triggers --show-field Package --no-field-names --regex '\b\('"${pkgregex}"'\)\b' | sed -e 's/$/ autopkgtest/'
}

get_reverse_build_dependencies() {
  # turn list of binaries into a pattern for grep-dctrl
  local pattern='\s\('
  local binary
  local patsep=''
  for binary in $(dh_listpackages); do
    pattern="$pattern""$patsep""$binary"
    patsep='\|'
  done
  pattern="$pattern"'\)\([, ]\|$\)'

  local rbdeps="$(chdist grep-dctrl-sources unstable -F Build-Depends,Build-Depends-Indep -r "$pattern" -n -s Package | sort -u)"

  for pkg in $rbdeps; do
    echo "$pkg rebuild"
  done
}

test_reverse_dependencies() {
  packages=${tmpdir}/testable-packages
  (get_reverse_dependencies && get_reverse_test_dependencies && get_reverse_build_dependencies) | sort | column -t > ${packages}
  if [ ! -s ${packages} ]; then
    echo "I: no reverse (build) dependencies"
    return
  fi

  if grep -q '\sno-autopkgtest$' ${packages}; then
    banner "Found reverse dependencies that cannot be tested automatically"
    awk '{ if ($2 == "no-autopkgtest") print($1) }' "${packages}" | column
    echo
    warning "Please proceed with care; maybe try manual tests"
  fi

  sed -i -e '/\sno-autopkgtest$/d' ${packages}
  if [ ! -s ${packages} ]; then
    return
  fi

  banner 'Found reverse runtime, build, or test dependencies that can be tested!'
  for kind in autopkgtest rebuild; do
    echo "$kind"
    echo "$kind" | sed -e 's/./-/g'
    echo
    awk '{ if ($2 == "'$kind'") print($1) }' "${packages}" | column
    echo
  done

  confirm "Which tests to run: [A(all)/e(dit list)]/s(kip all)] " a || true
  if [ "$confirm" = 's' ]; then
    warning "Skipping all remaining tests; please be careful!"
    return
  fi

  packages_to_test=${tmpdir}/packages-to-test
  if [ "$confirm" = 'e' ]; then
    (
      echo "########################################################################"
      echo "# Select which tests to run"
      echo "########################################################################"
      echo "#"
      echo "# Edit this file, and leave only the (package, test) pairs that you want to select."
      echo "# One (package, test) pair per line, separated by spaces"
      echo "# Remember to save the file if you make changes to the list."
      echo "# It's OK to leave comment or blank lines, they will be removed for you.'"
      echo "# The tests will run in the order they are listed in this file."
      echo "#"
      echo "########################################################################"
      cat "${packages}"
    ) > "$packages_to_test"
    ${EDITOR} "$packages_to_test"
    sed -i -e '/^\s*#/d; /^\s*$/d' "${packages_to_test}"
  else
    cp "${packages}" "${packages_to_test}"
  fi

  # list of locally built binaries to use
  local autopkgtest_debs="$global_autopkgtest_debs"
  local sbuild_extra_packages="$global_sbuild_extra_packages"
  for deb in $(dcmd --deb "$changes"); do
    autopkgtest_debs="$autopkgtest_debs $(readlink -f $deb)"
    sbuild_extra_packages="$sbuild_extra_packages --extra-package=$(readlink -f $deb)"
  done

  banner "Testing reverse (build) dependencies"

  failures="${tmpdir}/failures.txt"
  rc=0
  maxlength=$(ruby -n -e 'puts $_.split.first.length' ${packages_to_test} | sort -n  | tail -1)
  while read -r pkg test; do
    case "$test" in
      autopkgtest|rebuild)
        printf "%-12s %-${maxlength}s ... " "$test" "$pkg"
        thisrc=0
        if [ "$test" = "autopkgtest" ]; then
          log_file="${autopkgtest_logs}/${pkg}.log"
          run_autopkgtest --log-file="$log_file" ${autopkgtest_debs} "${pkg}" > /dev/null 2>&1 || thisrc=$?
        elif [ "$test" = "rebuild" ]; then
          log_file="${build_logs}/${pkg}.log"
          test_rebuild "$pkg" "$log_file" $sbuild_extra_packages || thisrc=$?
        fi
        if [ "$thisrc" -eq 0 ]; then
          echo "PASS"
        else
          echo "FAIL $log_file"
          echo "$pkg $test" >> "${failures}.in"
          rc=1
        fi
        ;;
      *)
        echo "E: invalid test type $test"
        ;;
    esac
  done < "${packages_to_test}"

  if [ $rc -eq 0 ]; then
    rm -rf "${tmpdir}"
  else
    column -t "${failures}.in" > "${failures}"
    rm -f "${failures}.in"
    warning "some tests failed; please be careful"
    echo "I: autopkgtest logs under $autopkgtest_logs"
    echo "I: rebuild logs under $build_logs"
    echo "I: list of all failures (so you can reuse it later) in $failures"
  fi
}

test_rebuild() {
  local pkg="$1"
  local log="$2"
  shift 2

  local tmpdir="$(mktemp -d)"
  cd "$tmpdir"
  chdist apt-get unstable source -qq --download-only "$pkg"

  local rc
  rc=0
  sbuild --quiet --arch-all --dist=unstable $@ *.dsc >/dev/null 2>&1 || rc=$?
  cp *_${arch}.build "$log"
  cd - > /dev/null
  rm -rf "$tmpdir"
  return "$rc"
}

confirm() {
  prompt="$1"
  default="${2:-n}"
  printf "$prompt"
  read confirm
  if [ -z "$confirm" ]; then
    confirm="$default"
  fi
  [ "$confirm" = 'y' ] || [ "$confirm" = 'Y' ]
}

ask_to_proceed() {
  if ! confirm "$@"; then
    echo "Aborted upon your request."
    exit 2
  fi
}

check_security_upload() {
  urgency=$(dpkg-parsechangelog -SUrgency)
  if [ "$urgency" != 'high' ]; then
    echo "Security upload should have urgency=high"
    exit 1
  fi
}

source=$(dpkg-parsechangelog -SSource)
version=$(dpkg-parsechangelog -SVersion | sed -e 's/^[0-9]\+://') # remove epoch
arch=$(dpkg --print-architecture)

tmpdir=$(mktemp --directory --tmpdir ${source}_${version}_${arch}.XXXXXXXXXX)
build_logs=${tmpdir}/buildlogs
autopkgtest_logs=${tmpdir}/autopkgtest
mkdir -p $build_logs $autopkgtest_logs

# target distribution/host
distribution=$(dpkg-parsechangelog -SDistribution)
git_builder=--git-builder=sbuild\ --arch-all\ --source\ --source-only-changes\ --verbose
case "$distribution" in
  *-security)
    git_builder="$git_builder"\ --git-ignore-branch
    if confirm "Security upload detected; include orig source? [Y/n]" y; then
      git_builder="$git_builder"\ --force-orig-source
    fi
    host=security-master
    check_security_upload
    ;;
  experimental)
    export SBUILD_CONFIG="$(dirname $(realpath $0))/sbuild_experimental.conf"
    git_builder="$git_builder"\ -c\ unstable-${arch}-sbuild\ --build-dep-resolver="aptitude"
    ;;
  UNRELEASED)
    git_builder="$git_builder"\ -c\ unstable-${arch}-sbuild
    ;;
  *-backports)
    git_builder="$git_builder"\ -d\ $distribution\ --build-dep-resolver=aptitude
    ;;
  *)
    git_builder="$git_builder"\ -d\ $distribution
    ;;
esac

gbp_buildpackage=gbp\ buildpackage\ --git-ignore-branch\ --git-export-dir=../build-area\ --git-builder=dpkg-buildpackage


global_sbuild_extra_packages=
global_autopkgtest_debs=
for pkg in $@; do
  deb=$(readlink -f $pkg)
  global_sbuild_extra_packages="$global_sbuild_extra_packages --extra-package=$deb"
  global_autopkgtest_debs="$global_autopkgtest_debs $deb"
done


# build
if [ "$BUILD" = 'yes' ]; then
  banner "Build"
  echo $gbp_buildpackage $git_builder $global_sbuild_extra_packages
  echo
  $gbp_buildpackage $git_builder $global_sbuild_extra_packages
fi

changes=${source}_${version}_${arch}.changes
if [ -e "../$changes" ]; then
  changes="../$changes"
else
  if [ -e "../build-area/$changes" ]; then
    changes="../build-area/$changes"
  else
    "echo E: $changes not found!"
    exit 1
  fi
fi

check_package 2>&1

if [ "$UPLOAD" = 'no' ]; then
  exit 0
fi

banner "Preparing to upload"

NEW=${NEW:-no}
if [ "$NEW" = yes ]; then
  SOURCE=no
else
  SOURCE=yes
fi

case "$distribution" in
  jessie-security)
    if grep-dctrl -q F Architecture all debian/control; then
      # source only uploads to jessie-security are not OK for Architecture: all
      # binary packages
      SOURCE=no
    fi
    ;;
esac

if [ $(grep -c "^${source}\s" debian/changelog) -eq 1 ]; then
  NEW=yes
  # must always upload binaries to NEW
  SOURCE=no
fi

# first backported version needs binaries uploaded
if [ $(expr match "$distribution" ".*-backports") -ne 0 ]; then
  previous=$(dpkg-parsechangelog --offset 1 --count 1 -S Distribution)
  if [ "$previous" != "$distribution" ]; then
    SOURCE=no
  fi
fi

if [ -n "${host:-}" ]; then
  printf "I: uploading to \033[38;1;1m${host}\033[m\n ..."
fi
echo "I: upload to NEW: ${NEW}"
echo "I: source upload: ${SOURCE}"
echo
ask_to_proceed "Proceed with the upload? [y/N] "


if [ $SOURCE = yes ]; then
  changes=${changes%%_${arch}.changes}_source.changes
fi

# sign
debsign "$changes"

# upload
dput ${DPUT:-} ${host:-} "$changes"

# tag
$gbp_buildpackage --git-tag-only --git-ignore-branch

# push
if [ $NEW = yes ]; then
  git push -u --all
else
  current_branch=$(git symbolic-ref HEAD | sed 's#refs/heads/##')
  if [ "$current_branch" != master -a ! -f .git/refs/remotes/origin/$current_branch ]; then
    git push -u origin "$current_branch"
  fi
  git push origin :
fi
git push --tags
