#!/bin/bash

GROUP="ruby-team"

set +e
#set -x

function get_attic()
{
  echo -e "\e[1mTesting for packages not longer in Debian\e[0m"
  g=100
  for ((i=0; i < ${#pkgs[@]}; i+=g))
  do
    part="${pkgs[@]:i:g}"
    #echo $part
    #continue
    listcur=$(rmadison -s unstable,experimental ${part[*]} | awk -F '|' '{print $1}' | tr -d '[:blank:]' | uniq)
    listold=$(rmadison -s oldoldstable,oldstable,stable ${part[*]} | awk -F '|' '{print $1}' | tr -d '[:blank:]' | uniq)
    list=$(echo "${listold}" | grep -vw "${listcur}")
    for p in ${list[@]}
    do
      echo -e "\e[31m${p}\e[0m is \e[1mNOT\e[0m in unstable or experimental but has been in the archive" 1>&2
    done
    test -n "${list}" && echo "${list}" >> .list_attic.log
  done
}


function get_not_in_debian()
{
  echo -e "\e[1mTesting for packages not in Debian\e[0m"
  g=100
  for ((i=0; i < ${#pkgs[@]}; i+=g))
  do
    part="${pkgs[@]:i:g}"
    #echo $part
    #continue
    pkglist=$(echo ${part} | sed -e 's/ /\n/g')
    output=$(rmadison ${part[*]} | awk -F '|' '{print $1}' | tr -d '[:blank:]' | uniq)
    list=$(echo "${pkglist}" | grep -vwe "${output}")
    for p in ${list[@]}
    do
      echo -e "\e[31m${p}\e[0m is \e[1mNOT\e[0m in any supported Debian release version" 1>&2
    done
    echo "${list}" >> .list_not_in_debian.log
  done
}

function print_result()
{
  logfile=$1
  header=$2
  echo
  echo "**************************************************************************"
  echo " ${header}: "
  echo "**************************************************************************"
  echo
  cat ${logfile} | egrep -vwf ${ignore} | sort
}

echo -n "Retreiving package list ..."
if [ $# -ne 0 ]
then
  pkgs="$@"
else
  if [ -f 'projects.list' ]
  then
    mapfile -t pkgs < projects.list
  else
    mapfile -t pkgs < <(gitlab-api-v4 -a group_projects ${GROUP} | jq -r '.[] | .name' | sort)
  fi
fi
echo " done"

ignore="$(dirname `readlink -f $0`)/ignore-attic.list"
if [ ! -e $ignore ]
then
  ignore="/dev/null"
fi

rm -f .list_attic.log
rm -f .list_not_in_debian.log
touch .list_attic.log || exit 1
touch .list_not_in_debian.log || exit 1

get_attic ${pkgs[@]}
get_not_in_debian ${pkgs[@]}

print_result '.list_attic.log' 'Attic candidates'
print_result '.list_not_in_debian.log' 'Repositories of packages not in Debian at all'

exit 0

# vim: set ts=2 sw=2 ai si et:
